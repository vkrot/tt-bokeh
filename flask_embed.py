from threading import Thread
import numpy as np
from flask import Flask, render_template
from tornado.ioloop import IOLoop

from bokeh.embed import server_document
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Slider
from bokeh.plotting import figure, output_file
from bokeh.server.server import Server
from bokeh.themes import Theme
from bokeh.driving import count
from bokeh.layouts import column, gridplot, row
from bokeh.models import ColumnDataSource, Select, Slider
from bokeh.plotting import curdoc, figure
import pandas as pd
from math import pi
from urllib.parse import urlparse
from flask import request
import json
from io import StringIO
from datetime import datetime

app = Flask(__name__)

_doc = None
_title = 'Sea Surface Temperature at 43.18, -70.43'
_source = None

df0 = pd.read_csv('BTCUSDT.csv', sep=';')
df0['RSI_14']=0.5
df0['up'] = df0['close']+100
df0['down'] = df0['close']-100
df = df0[0:100]
df['time'] = pd.to_datetime(df['time'], unit='ms')
df['date'] = df['time']
_data = df[0:100]
_has_updates = False
_has_trades_updates = False
_trades_data_b = {'idx': [], 'price': []}
_trades_data_s = {'idx': [], 'price': []}

_last_update = datetime.utcnow()

def bkapp(doc):
    global _data
    global _source
    source = ColumnDataSource(df)
    _source = source

    # inc = df.close > df.open
    # dec = df.open > df.close

    TOOLS = "pan,wheel_zoom,box_zoom,reset,save"

    # p = figure(x_axis_type="datetime", tools=TOOLS, plot_width=1000, title = "MSFT Candlestick")
    p = figure(tools=TOOLS, plot_width=1000, title = "MSFT Candlestick")
    p.xaxis.major_label_orientation = pi/4
    p.grid.grid_line_alpha=0.3
    # start_date = df.iloc[0]['date']
    # end_date = df.iloc[len(df)-1]['date']

    #     w = 12*60*60*1000 # half day in ms
    # p.segment(df.date, df.high, df.date, df.low, color="black")
    # p.vbar(x=df.date[inc], bottom=df.open[inc], top=df.close[inc], width=20, fill_color="#D5E1DD", line_color="black")
    # p.vbar(x=df.date[dec], bottom=df.open[dec], top=df.close[dec], width=20, fill_color="#F2583E", line_color="black")

    # p.line(x=df.index, y=df.close, color='black')

    global _trades_data_b
    global _trades_data_s
    source_b = ColumnDataSource(_trades_data_b)
    source_s = ColumnDataSource(_trades_data_s)

    p.line(x='index', y='close', color='black', source=source)
    p.scatter(x='idx', y='price',color='green', source=source_b)
    p.scatter(x='idx', y='price',color='red', source=source_s)

    p2 = figure(plot_height=250, x_range=p.x_range, tools="xpan,xwheel_zoom,xbox_zoom,reset", y_axis_location="right")
    p2.line(x='index', y='RSI_14', color='red', source=source)

    @count()
    def update(t):
        global _data
        import random
        r = random.randrange(0, 100)
        # _data = df[r:r+100]
        # _data['close'] = _data['close'] + r
        #_data

        global _has_updates
        global _has_trades_updates
        global df
        if _has_updates:
            source.data = ColumnDataSource.from_df(df)

        global _trades_data_b
        global _trades_data_s
        if _has_trades_updates:
            source_b.data = _trades_data_b
            source_s.data = _trades_data_s

        _has_updates = False
        _has_trades_updates = False
        #source.stream(_data, len(_data))

    doc.add_root(column(gridplot([[p], [p2]], toolbar_location="left", plot_width=1000)))
    doc.add_periodic_callback(update, 500)
    doc.title = "OHLC"


    # df = sea_surface_temperature.copy()
    # source = ColumnDataSource(data=df)
    # global _title
    # global _source
    # _source = source
    # plot = figure(x_axis_type='datetime', y_range=(0, 25), y_axis_label='Temperature (Celsius)',
    #               title=_title)
    # plot.line('time', 'temperature', source=source)
    #
    # def callback(attr, old, new):
    #     if new == 0:
    #         data = df
    #     else:
    #         data = df.rolling('{0}D'.format(new)).mean()
    #     source.data = ColumnDataSource.from_df(data)
    #
    # slider = Slider(start=0, end=30, value=0, step=1, title="Smoothing by N Days")
    # slider.on_change('value', callback)
    #
    # global _doc
    # _doc = doc
    # doc.add_root(column(slider, plot))

    # doc.theme = Theme(filename="theme.yaml")




@app.route('/', methods=['GET'])
def bkapp_page():
    o = urlparse(request.base_url)
    host = o.hostname
    script = server_document(f'http://{host}:5006/bkapp')
    #script = server_document(relative_urls=True)
    return render_template("embed.html", script=script, template="Flask")


@app.route('/update-trades', methods=['POST'])
def update():
    data = request.get_json(force=True)
    print('/update-trades:')
    print(data)

    buy_idx = data['buy_idx']
    buy_price = data['buy_price']

    sell_idx = data['sell_idx']
    sell_price = data['sell_price']

    global _has_trades_updates
    _has_trades_updates = True

    global _trades_data_b
    global _trades_data_s
    _trades_data_b = {
        'idx': buy_idx,
        'price': buy_price,
    }
    _trades_data_s = {
        'idx': sell_idx,
        'price': sell_price,
    }

    global _last_update
    _last_update = datetime.utcnow()

    # global _source
    # if _source:
        # _source.data = ColumnDataSource.from_df(df)
    return 'ok'

@app.route('/update-df', methods=['POST'])
def update2():
    data = request.data.decode('utf-8')
    print(data)
    df0 = pd.read_csv(StringIO(data))
    print(df0)
    global df
    df = df0
    global _source

    global _has_updates
    _has_updates = True

    # if _source:
        # _source.data = ColumnDataSource.from_df(df)

    return 'ok'

@app.route('/last-update', methods=['GET'])
def last_upd():
    global _last_update

    return str(_last_update)

def bk_worker():
    # Can't pass num_procs > 1 in this configuration. If you need to run multiple
    # processes, see e.g. flask_gunicorn_embed.py
    server = Server({'/bkapp': bkapp}, io_loop=IOLoop(), allow_websocket_origin=["localhost:8000"])
    server.start()
    server.io_loop.start()

Thread(target=bk_worker).start()

if __name__ == '__main__':
    print('Opening single process Flask app with embedded Bokeh application on http://localhost:8000/')
    print()
    print('Multiple connections may block the Bokeh app in this configuration!')
    print('See "flask_gunicorn_embed.py" for one way to run multi-process')
    app.run(host='0.0.0.0', port=8000)
