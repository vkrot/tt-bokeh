#!/usr/bin/env python
# coding: utf-8

# In[41]:


#csv_file = '/Users/vkrot/workspace/dumps/rl/klines/BTCUSDT.csv'
csv_file = 'https://gitlab.com/vkrot/tt-bokeh/raw/master/data/BTCUSDT.csv'
#host = 'localhost'
host = '157.245.82.226'


# In[47]:


import os
import sys
import warnings
import numpy

def warn(*args, **kwargs):
    pass

warnings.warn = warn
warnings.simplefilter(action='ignore', category=FutureWarning)
numpy.seterr(divide = 'ignore') 

sys.path.append(os.path.dirname(os.path.abspath('')))


# In[44]:


import tensorflow as tf
import pandas as pd

from stable_baselines.common.policies import MlpLnLstmPolicy
from stable_baselines import PPO2

from tensortrade.strategies import StableBaselinesTradingStrategy
from tensortrade.environments import TradingEnvironment
from tensortrade.rewards import RiskAdjustedReturns
from tensortrade.actions import ManagedRiskOrders
from tensortrade.instruments import Quantity, TradingPair, BTC, USD
from tensortrade.orders.criteria import Stop, StopDirection
from tensortrade.wallets import Wallet, Portfolio
from tensortrade.exchanges.simulated import SimulatedExchange
from tensortrade.features.stationarity import LogDifference
from tensortrade.features.scalers import MinMaxNormalizer
from tensortrade.features import FeaturePipeline

WINDOW_SIZE = 1
PRICE_COLUMN = 'close'

normalize = MinMaxNormalizer(inplace=True)
difference = LogDifference(inplace=True)
feature_pipeline = FeaturePipeline(steps=[normalize])

action_scheme = ManagedRiskOrders(pairs=[USD/BTC])
reward_scheme = RiskAdjustedReturns(return_algorithm="sortino")

ohlcv_data = pd.read_csv(csv_file, index_col="time")
ohlcv_data = ohlcv_data[['close']]

exchange = SimulatedExchange(data_frame=ohlcv_data, price_column=PRICE_COLUMN, randomize_time_slices=True)

wallets = [(exchange, USD, 10000), (exchange, BTC, 0)]

portfolio = Portfolio(base_instrument=USD, wallets=wallets)

environment = TradingEnvironment(exchange=exchange,
                                 portfolio=portfolio,
                                 action_scheme=action_scheme,
                                 reward_scheme=reward_scheme,
                                 feature_pipeline=feature_pipeline,
                                 window_size=WINDOW_SIZE,
                                 observe_wallets=[USD, BTC])

print('Observation Data:')
print(environment.observation_columns)

model = PPO2
policy = MlpLnLstmPolicy
params = { "learning_rate": 1e-5, 'nminibatches': 1, "tensorboard_log": '/Users/vkrot/tb/'}

strategy = StableBaselinesTradingStrategy(environment=environment,
                                          model=model,
                                          policy=policy,
                                          model_kwargs=params)


# In[50]:


import requests
from tensortrade.trades import Trade, TradeSide

def update_df(df: pd.DataFrame):
    csv = df.to_csv()
    url = f'http://{host}:8000/update-df'
    try:
        res = requests.post(url, data = csv)
        print(f'Update df status code: {res.status_code}')
    except Exception:
        print('Failed to send df')

    

last_trades = None
def render(df: pd.DataFrame):
    print('------------ Trades:')
    import json
    print(environment.episode_trades)
    trades = environment.episode_trades.values()
    trades = [item for sublist in trades for item in sublist]
    global last_trades
    if last_trades != trades:
        buys = [t for t in trades if t.side == TradeSide.BUY]
        sells = [t for t in trades if t.side == TradeSide.SELL]
        buy_idx = [t.step for t in buys]
        buy_price = [t.price for t in buys]

        sell_idx = [t.step for t in sells]
        sell_price = [t.price for t in sells]
        
        data = {
            'buy_idx': buy_idx,
            'buy_price': buy_price,
            'sell_idx': sell_idx,
            'sell_price': sell_price,            
        }
        print(json.dumps(data))

        url = f'http://{host}:8000/update-trades'
        try:
            res = requests.post(url, data = json.dumps(data))
            print(f'Update trades status code: {res.status_code}')
        except Exception as e:
            print('Failed to send trades:')
            print(e)
        
    return False


# In[49]:


update_df(ohlcv_data)
performance = strategy.run(steps=10000, episode_callback=render)
print('Training finished')
print('Performace:')
print(performance)


# In[ ]:


strategy.evaluate(episodes=1, render_mode='chart')


# In[ ]:


performance


# In[ ]:


environment.episode_trades


# In[ ]:


performance.net_worth.plot()


# In[ ]:


performance

